## Problem

*Caused by: javax.net.ssl.SSLHandshakeException: Received fatal alert: handshake_failure*

Solution references:

1. Download extra jar: https://stackoverflow.com/questions/38203971/javax-net-ssl-sslhandshakeexception-received-fatal-alert-handshake-failure
2. JAVA_HOME: https://stackoverflow.com/questions/6588390/where-is-java-home-on-osx-sierra-10-12-el-captain-10-11-yosemite-10-10
