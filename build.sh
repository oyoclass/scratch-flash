#!/usr/bin/env bash
# Scratch.as
# scratch/ScratchRuntime.as
# ui/parts/StagePart.as
# util/Server.as
# util/ProjectIO.as
# To log into console: jSThrowError()
./gradlew build -Ptarget=11.6 --stacktrace
