window.scratchEncode = function(buffer, position, len) {
    if(typeof(len) === "undefined") {
        len = buffer.byteLength;
    }
    if(typeof(position) === "undefined") {
        position = 0;
    }
    var alphabet = "abcdefghijklmnopqrstuvwxyz";
    var len = Math.min(len, buffer.byteLength-(position));
    position = position-1;
    var dataView = new DataView(buffer);
    var result = "";
    var n = 0;
    len = len + position + 1;
    while(++position <  len){
        n = 128 + dataView.getUint8(position);
        result += alphabet.charAt(n / 16) + alphabet.charAt(n % 16) ;
    }
    return result;
};