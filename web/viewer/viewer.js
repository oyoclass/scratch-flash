(function() {
    "use strict";

    var scratch = document.getElementById("scratch");
    window.scratch = scratch;

    var fileInput = document.getElementById("file-input");
    fileInput.addEventListener("change", function(e) {
        var file = e.target.files[0];
        if (!file) {
            return;
        }
        var fileName = fileInput.value.split(/(\\|\/)/g).pop();
        var reader = new FileReader();
        reader.readAsArrayBuffer(file);
        reader.onload = function(e) {
            scratch.ASinstallProject(fileName, scratchEncode(reader.result, 0, reader.result.byteLength));
        };
    }, false);
})();