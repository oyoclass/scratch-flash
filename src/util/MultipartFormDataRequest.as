package util {
    import flash.net.URLRequest;
    import flash.net.URLRequestHeader;
    import flash.net.URLRequestMethod;
    import flash.utils.ByteArray;

public class MultipartFormDataRequest {
    public var url:String;
    private var _data:ByteArray;
    private var _boundaryParam:String;

    public function get data():ByteArray { return _data; }
    public function get boundaryParameter():String { return _boundaryParam; }
    public function get contentType():String { return "multipart/form-data; boundary=" + boundaryParameter; }

    public function MultipartFormDataRequest(url:String, data:ByteArray, boundaryParam:String) {
        this.url = url;
        _data = data;
        _boundaryParam = boundaryParam;
    }

    public function toURLRequest():URLRequest {
        var request:URLRequest = new URLRequest(url);
        request.data = data;
        request.method = URLRequestMethod.POST;
        request.contentType = contentType;
        return request;
    }
}
}