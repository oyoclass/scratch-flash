package util {
    import flash.utils.ByteArray;
    import flash.utils.setTimeout;

    public class MultipartFormDataRequestBuilder {
        private static const LINEBREAK:String = "\r\n";
        private static const LINEBREAK_BYTES:ByteArray = stringToBytes(LINEBREAK);
        private static const BOUNDARY_AFFIX:String = "--";
        private static const BOUNDARY_AFFIX_BYTES:ByteArray = stringToBytes(BOUNDARY_AFFIX);

        public var url:String = null;
        public var encoding:String = "utf-8";

        private var parts:Array = [];

        public function MultipartFormDataRequestBuilder(url:String = null) {
            this.url = url;
        }

        public function addParameter(name:String, value:String):void {
            parts.push({
                isFile: false,
                name: stringToBytes(name),
                value: stringToBytes(value, encoding)
            });
        }

        public function addFile(name:String, file:ByteArray, fileName:String, contentType:String="application/octet-stream"):void {
            parts.push({
                isFile: true,
                name: stringToBytes(name),
                value: file,
                fileName: stringToBytes(fileName),
                contentType: stringToBytes(contentType)
            });
        }

        /**
         * Constructs the request using the parameters and files added to the builder in order.
         * If a callback funciton is supplied, this function will run asynchronously.
         * Otherwise, this function will run synchronously.
         *
         * @param whenDone
         *
         * @return The MultipartFormDataRequest if running synchronously, or null otherwise.
         */
        public function buildRequest(whenDone:Function=null, parameterDelay:uint=1, fileDelay:uint=1):MultipartFormDataRequest {
            var data:ByteArray = new ByteArray();
            var boundaryParam:String = generateBoundaryParameter();
            var boundaryParamBytes:ByteArray = stringToBytes(boundaryParam);
            var boundary:String = "--" + boundaryParam + "\r\n";
            var boundaryBytes:ByteArray = stringToBytes(boundary);

            var partSegments:Array = [];
            partSegments.push(stringToBytes('Content-Disposition: form-data; name="'));
            partSegments.push(stringToBytes('"'));
            partSegments.push(stringToBytes('; filename="'));
            partSegments.push(stringToBytes('"' + LINEBREAK + 'Content-Type: '));

            function writePartAsync(index:uint):void {
                if(index >= parts.length) {
                    writeEnd();
                    whenDone(new MultipartFormDataRequest(url, data, boundaryParam));
                    return;
                }

                var part:Object = parts[index];
                writePart(part);
                var delay:uint = parameterDelay;
                if(part.isFile) {
                    delay = fileDelay;
                }
                setTimeout(writePartAsync, delay, index + 1);
            }

            function writePart(part:Object):void {
                data.writeBytes(boundaryBytes);

                data.writeBytes(partSegments[0]);
                data.writeBytes(part.name);

                data.writeBytes(partSegments[1]);
                if(part.isFile) {
                    data.writeBytes(partSegments[2]);
                    data.writeBytes(part.fileName);

                    data.writeBytes(partSegments[3]);
                    data.writeBytes(part.contentType);
                }

                data.writeBytes(LINEBREAK_BYTES);
                data.writeBytes(LINEBREAK_BYTES);
                data.writeBytes(part.value);
                data.writeBytes(LINEBREAK_BYTES);
            }

            function writeEnd():void {
                data.writeBytes(BOUNDARY_AFFIX_BYTES);
                data.writeBytes(boundaryParamBytes);
                data.writeBytes(BOUNDARY_AFFIX_BYTES);
                data.writeBytes(LINEBREAK_BYTES);
            }

            if(whenDone == null) {
                for(var i:uint = 0; i < parts.length; i++) {
                    writePart(parts[i]);
                }
                writeEnd();
                return new MultipartFormDataRequest(url, data, boundaryParam);
            }
            // else
            writePartAsync(0);
            return null;
        }

        private static function stringToBytes(value:String, encoding:String="ascii"):ByteArray {
            var byteArray:ByteArray = new ByteArray();
            writeString(byteArray, value, encoding);
            return byteArray;
        }

        private static function writeString(array:ByteArray, value:String, encoding:String="ascii"):void {
            array.writeMultiByte(value, encoding);
        }

        private static function generateBoundaryParameter(base:String = ""):String {
            var boundaryAlphabetNoSpace:String = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'()+,-./:=?";
            var boundaryAlphabet:String = boundaryAlphabetNoSpace + " ";
            var boundaryParam:String = base;
            var boundaryParamLength:uint = 69 - boundaryParam.length; // Must be between 0 and 69
            for(var i:uint = 0; i < boundaryParamLength; i++) {
                boundaryParam += boundaryAlphabet.charAt(int(Math.random() * boundaryAlphabet.length));
            }
            boundaryParam += boundaryAlphabetNoSpace.charAt(int(Math.random() * boundaryAlphabetNoSpace.length));
            return boundaryParam;
        }
    }
}